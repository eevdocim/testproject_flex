<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected UserRepository $userRepository;
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function create(
        string $name,
        string $email,
        string $password
    ) {
        $user = $this->userRepository->create([
            'name'      => $name,
            'email'     => $email,
            'password'  => Hash::make($password)
        ]);

        event(new Registered($user));

        return $user;
    }
}

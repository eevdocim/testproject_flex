<?php

namespace App\Services;

use App\Repositories\EquipmentRepository;

class EquipmentService
{
    protected EquipmentRepository $equipmentRepository;
    public function __construct(
        EquipmentRepository $equipmentRepository
    ) {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function create(
        int $type_id,
        string $serial_number,
        string $note
    ) {
        $equipment = $this->equipmentRepository->create([
            'type_id'       => $type_id,
            'serial_number' => $serial_number,
            'note'  => $note
        ]);

        return $equipment;
    }
}

<?php

namespace App\Services;

use App\Repositories\EquipmentTypeRepository;

class EquipmentTypeService
{
    protected EquipmentTypeRepository $equipmentTypeRepository;
    public function __construct(
        EquipmentTypeRepository $equipmentTypeRepository
    ) {
        $this->equipmentTypeRepository = $equipmentTypeRepository;
    }

    public function create(
        string $name,
        string $mask
    ) {
        $type = $this->equipmentTypeRepository->create([
            'type_id'            => $name,
            'serial_number_mask' => $mask
        ]);

        return $type;
    }
}

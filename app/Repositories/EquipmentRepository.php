<?php

namespace App\Repositories;

use App\Models\Equipment;

class EquipmentRepository
{
    public function create($attributes)
    {
        return Equipment::create($attributes);
    }

    public function getById(int $id)
    {
        return Equipment::find($id);
    }
}

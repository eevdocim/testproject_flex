<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function create($attributes)
    {
        return User::create($attributes);
    }

    public function getById(int $id)
    {
        return User::find($id);
    }
}

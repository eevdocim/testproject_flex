<?php

namespace App\Repositories;

use App\Models\EquipmentType;

class EquipmentTypeRepository
{
    public function create($attributes)
    {
        return EquipmentType::create($attributes);
    }

    public function getById(int $id)
    {
        return EquipmentType::find($id);
    }
}

<?php

namespace Database\Seeders;

use App\Services\UserService;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function __construct(
        UserService $userService
    ) {
        $this->userService = $userService;
    }
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        echo "Add user test, test@test, test\n";
        $this->userService->create('test', 'test@test', 'test');
    }
}
